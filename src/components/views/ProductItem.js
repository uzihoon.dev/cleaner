import React from "react";
import { Image, Text, TouchableOpacity, StyleSheet, View } from "react-native";
import colors from "../../colors";
import fonts from "../../fonts";
import imgPlaceholder from "../../../assets/images/abc.png";

const ProductItem = ({ item, margin, onPress, onPressDelete }) => {
  const { image } = styles;

  return (
    <View>
      <TouchableOpacity onPress={onPress}>
        <Image
          style={[{ marginRight: margin }, image]}
          source={item.imageUrl ? { url: item.imageUrl } : null}
          defaultSource={imgPlaceholder}
        />
        <View>
          <Text>{item.name}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({

})

export { ProductItem };
