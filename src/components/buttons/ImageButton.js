import React from "react";
import { StyleSheet, Image, TouchableOpacity } from "react-native";

const ImageButton = ({ style, size, source, onPress }) => {
  const { image, container } = styles;
  return (
    <TouchableOpacity style={[container, style]} onPress={onPress}>
      <Image style={[{ width: size, height: size }, image]} source={source} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {},
  image: {
    margin: 5
  }
});

export { ImageButton };
