import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import fonts from "../../fonts";

const Button = ({
  style,
  backgroundColor,
  textColor,
  title,
  enable = true,
  onPress
}) => {
  const { container, text } = styles;

  return (
    <TouchableOpacity
      style={[
        {
          opacity: enable ? 1 : 0.3,
          backgroundColor
        },
        container,
        style
      ]}
      disabled={!enable}
      onPress={onPress}>
      <Text style={[{ color: textColor }, text]}>{title}</Text>
    </TouchableOpacity>
  );
};

const margin = 14;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    borderRadius: 5,
    margin,
    height: 50
  },
  text: {
    fontFamily: fonts.regular,
    fontSize: 16,
    margin
  }
});

export { Button };
