import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Router, Stack, Scene } from "react-native-router-flux";

import HomeScreen from "./screens/HomeScreen";
import TodoScreen from "./screens/TodoScreen";
import NumberScreen from "./screens/NumberScreen";
import LaunchScreen from "./screens/LaunchScreen";
import colors from "./colors";

class RouterComponent extends Component {
  render() {
    const { container } = styles;
    return (
      <Router sceneStyle={container}>
        <Stack key="root">
          <Stack hideNavBar>
            <Scene component={LaunchScreen} />
          </Stack>
          <Stack key="number" hideNavBar>
            <Scene component={HomeScreen} key="home" />
            <Scene component={TodoScreen} key="press"/>
          </Stack>
        </Stack>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white
  }
});

export default RouterComponent;
