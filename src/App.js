import React, { Component } from "react";
import { Provider } from "react-redux";
import { View, StatusBar } from "react-native";
import createStore from "./store";
import Router from "./Router";
import * as colors from "./colors";

const store = createStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <StatusBar barStyle="default" backgroundColor={colors.black} />
          <Router />
        </View>
      </Provider>
    );
  }
}

export default App;
