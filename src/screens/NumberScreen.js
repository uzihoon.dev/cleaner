import React, { Component } from "react";
import { Text, Button, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as baseActions from "../store/modules/base";

class HomeScreen extends Component {
  componentDidMount() {
    console.log("이건 넘버 스크린");
  }

  componentDidUpdate() {
    const { number } = this.props;
    console.log(number);
  }

  handleIncrement = () => {
    const { BaseActions } = this.props;
    BaseActions.increment(10);
  };

  handleDecrement = () => {
    const { BaseActions } = this.props;
    BaseActions.decrement(10);
  };
  render() {
    const { number } = this.props;
    return (
      <View>
        <Text style={styles.text}>{number}</Text>
        <Button title="INCREMENT" onPress={this.handleIncrement} />
        <Button title="DECREMENT" onPress={this.handleDecrement} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 60
  }
});

export default connect(
  state => ({
    number: state.base.number
  }),
  dispatch => ({
    BaseActions: bindActionCreators(baseActions, dispatch)
  })
)(HomeScreen);
