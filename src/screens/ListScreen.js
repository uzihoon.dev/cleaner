import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { LoadingView, ProductItem } from "../components";
import colors from "../colors";

class ListScreen extends Component {
  renderLoading = () => {
    return <LoadingView />;
  }

  renderList = () => {
    const { container, flatList } = styles;

    return (
      <View style={container}>
        <FlatList
          contentContainerStyle={flatList}
          data={}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <ProductItem
              margin={padding}
              item={item}
              onPress={() => Actions.product({ product: item })}
              // onPressDelete
            />
          )}
        />
      </View>
    )
  }
}


const padding = 14;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  flatList: {
    paddingTop: padding / 2,
    paddingBottom: padding / 2,
    paddingLeft: padding,
    paddingRight: padding
  }
});

