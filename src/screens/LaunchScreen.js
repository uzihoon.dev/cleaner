import React, { Component } from "react";
import { Animated, StyleSheet, Image, View } from "react-native";
import { connect } from "react-redux";
import colors from "../colors";
import fonts from "../fonts";
import backgroundImg from "../../assets/images/test.jpg";
import { Button } from "../components";
import { Actions } from "react-native-router-flux";

const PLUSE = 0.1;
const DURATION = 800;

class LaunchScreen extends Component {
  state = {
    animated: new Animated.Value(1 - PLUSE)
  };

  componentWillMount() {
    // this.props.configure();
    // this.props.tryToSignInSilently();
  }

  componentDidMount() {
    this.fadeIn();
  }

  fadeIn = () => {
    const { animated } = this.state;

    Animated.timing(animated, {
      toValue: 1 + PLUSE,
      duration: DURATION
    }).start(() => this.fadeOut());
  };

  fadeOut = () => {
    const { animated } = this.state;

    Animated.timing(animated, {
      toValue: 1 - PLUSE,
      duration: DURATION
    }).start(() => this.fadeIn());
  };

  handleLogin = () => {
    Actions.number();
    console.log("AAA!");
  }

  render() {
    const { animated } = this.state;
    const { container, welcome, bottom, fullImage, button } = styles;

    return (
      <View style={container}>
        <View style={welcome}>
          <Image source={backgroundImg} style={fullImage} />
          {/* <Animated.View style={{ transform: [{ scale: animated }] }}> */}
          {/* <Text style={title}>LV3 WHITE</Text> */}
          {/* </Animated.View> */}
          <View style={bottom}>
            <Button
              title={"LOGIN"}
              backgroundColor={colors.gray}
              style={button}
              textColor={colors.white}
              onPress={this.handleLogin}
            />
          </View>
        </View>
      </View>
    );
  }
}

const margin = 14;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.white,
    margin
  },
  welcome: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "relative"
  },
  fullImage: {
    position: "absolute",
    flex: 1,
    zIndex: 0
  },
  image: {
    height: 100,
    width: 100,
    margin
  },
  title: {
    fontFamily: fonts.regular,
    color: colors.white,
    fontSize: 24,
    zIndex: 1,
    margin
  },
  button: {
    width: 300
  },
  bottom: {
    position: "absolute",
    bottom: 100,
    flex: 1
  }
});

export default connect()(LaunchScreen);
