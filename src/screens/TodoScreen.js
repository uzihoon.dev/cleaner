import React, { Component } from "react";
import firebase from "react-native-firebase";
import { FlatList, View, Text, TextInput, Button } from "react-native";
import { Todo, LoadingView } from "../components";

class Todos extends Component {
  constructor() {
    super();
    this.ref = firebase.firestore().collection("toods");
    this.unsubscribe = null;

    this.state = {
      textInput: "",
      loading: true,
      todos: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  componentWillUnMount() {
    this.unsubscribe();
  }

  onCollectionUpdate = querySnapshot => {
    const todos = [];
    querySnapshot.forEach(doc => {
      const { title, complete } = doc.data();

      todos.push({
        key: doc.id,
        doc,
        title,
        complete
      });
    });

    this.setState({
      todos,
      loading: false
    });
  };

  updateTextInput(value) {
    this.setState({ textInput: value });
  }

  addTodo() {
    this.ref.add({
      title: this.state.textInput,
      complete: false
    });
    console.log(this.state.textInput);

    this.setState({
      textInput: ""
    })
  }

  render() {
    if (this.state.loading) {
      return <LoadingView />;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.todos}
            renderItem={({ item }) => <Todo {...item} />}
          />
          <TextInput
            placeholder={"ADD TODO"}
            value={this.state.textInput}
            onChangeText={text => this.updateTextInput(text)}
          />
          <Button
            title={"ADD TODO"}
            disabled={!this.state.textInput.length}
            onPress={() => this.addTodo()}
          />
        </View>
      );
    }
  }
}

export default Todos;
