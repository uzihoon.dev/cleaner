import { createAction, handleActions } from "redux-actions";
import produce from "immer";

const INCREMENT = "base/INCREMENT";
const DECREMENT = "base/DECREMENT";
const SAVE_DATA = "base/SAVE_DATA";

export const increment = createAction(INCREMENT);
export const decrement = createAction(DECREMENT);
export const saveData = createAction(SAVE_DATA);

const initialState = { number: 0 };

export default handleActions(
  {
    [SAVE_DATA]: (state, action) => {
      const { payload: number } = action;
      return produce(state, draft => {
        draft.number = number;
      });
    }
  },
  initialState
);
