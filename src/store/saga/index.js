import { fork, all, select, takeEvery, put } from "redux-saga/effects";
import * as baseActions from "../modules/base";

const getBaseDataFromStore = state => state.base;

export default function* rootSaga() {
  yield all([
    fork(handleBase)
  ])
}

function* handleBase() {
  yield takeEvery(baseActions.increment, increment);
  yield takeEvery(baseActions.decrement, decrement);
}

function* increment(action) {
  const { payload: add } = action;
  const baseData = yield select(getBaseDataFromStore);
  const number = baseData.number + add;
  yield put(baseActions.saveData(number));
}

function* decrement(action) {
  const { payload: add } = action;
  const baseData = yield select(getBaseDataFromStore);
  const number = baseData.number - add + 5;
  yield put(baseActions.saveData(number));
}