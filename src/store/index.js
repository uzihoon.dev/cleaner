import { combineReducers } from "redux";
import * as modules from "./modules";
import saga from "./saga";
import configure from "./configure";

const reducers = combineReducers(modules);

export default () => {
  return configure(reducers, saga);
};
